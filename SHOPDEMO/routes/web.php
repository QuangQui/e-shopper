<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Frontend

Route::group(['prefix'=>'frontend','namespace'=>'Frontend', 'as'=>'frontend.'], function(){

    Route::get('/home','HomeController@index')->name('home');
    
    // BLOG
    Route::group(['prefix' => 'blog'], function(){
        Route::get('/index','BlogController@index')->name('blog');
        Route::get('/blog-single/{id}','BlogController@showBlogSingle')->name('blogSingle');
        Route::post('/blog-rate','BlogController@blogRate')->name('blogRate');
        Route::post('/blog-single/{id}','CommentsController@store')->name('cmtBlog');
    });

    // MEMBER
    Route::group(['prefix'=> 'member'], function(){
        Route::get('/login','MemberController@login')->name('login');
        Route::post('/login','MemberController@postLogin')->name('postLogin');
        Route::get('/logout','MemberController@logout')->name('logout');
        Route::get('/register','MemberController@register')->name('register');
        Route::post('/register','MemberController@postRegister')->name('postRegister');
        Route::get('/account','MemberController@show')->name('showAccount');
        Route::post('/account','MemberController@update')->name('updateAccount');
    });

    // PRODUCT
    Route::group(['prefix' => 'product'], function(){
        Route::get('/index','ProductController@show')->name('showProduct');
        Route::post('/search','ProductController@store')->name('searchProduct');
        Route::get('/price-range','ProductController@priceRange')->name('priceRange');
        Route::get('/index-shop','ProductController@showShop')->name('showShop');
    });

    // CART
    Route::group(['prefix' => 'cart'], function(){
        Route::get('/index','CartController@index')->name('cart');
        Route::post('/add-to-cart','CartController@store')->name('addToCart');
        Route::post('/cart-quanty-up','CartController@cartQtyUp')->name('cartQtyUp');
        Route::post('/cart-quanty-down','CartController@cartQtyDown')->name('cartQtyDown');
        Route::get('/delete-cart','CartController@destroy')->name('destroyCart');
    });

    // checkout
    Route::group(['prefix' => 'checkout'], function(){
        Route::get('/index','CheckoutController@index')->name('checkout');
        Route::get('/success','CheckoutController@show')->name('checkoutSuccess');
        Route::get('/send-mail','CheckoutController@store')->name('sendMail');
        Route::post('/save-user','CheckoutController@store')->name('saveHistory');
    });

    // check login 
    Route::group(['middleware' => 'member'], function () {
        Route::get('product/add','ProductController@create')->name('addProduct');
        Route::post('product/add','ProductController@createProduct')->name('createProduct');
        Route::get('product/edit/{id}','ProductController@update')->name('editProduct');
        Route::post('product/edit/{id}','ProductController@updateProduct')->name('updateProduct');
        Route::get('product/delete/{id}','ProductController@destroy')->name('deleteProduct');
        Route::get('product/details/{id}','ProductController@index')->name('detailProduct');
    });



    // Send-mail
    Route::get('/index','MailController@index')->name('indexMail');

    Route::get('/contact','ContactController@index')->name('contact');

    Route::get('/error','ErrorController@index')->name('error');

});

// Admin

Auth::routes();

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    Route::get('/', 'LoginController@showLoginForm');
    Route::get('/login', 'LoginController@showLoginForm');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout');
});


Route::group([
    'prefix'=> 'admin', // tiền tố vào sau link
    'namespace'=>'Admin',
    'as'=>'admin.', 
    'middleware' => ['admin']
], function(){
//dashboard
    Route::get('/dashboard','DashboardController@index')->name('dashboard');

// user
    Route::get('/update','UserController@profile')->name('profile');
    Route::post('/update','UserController@update')->name('updateProfile');

// country
    Route::get('/country','CountryController@country')->name('country');
    Route::get('/add-country','CountryController@addCountry')->name('addCountry');
    Route::get('/delete-country/{id}','CountryController@deleteCountry')->name('deleteCountry');
    Route::post('/add-country','CountryController@createCountry')->name('createCountry');

// category
    Route::get('/category','CategoryController@show')->name('category');
    Route::get('/add-category','CategoryController@create')->name('addCategory');
    Route::get('/delete-category/{id}','CategoryController@destroy')->name('deleteCategory');
    Route::post('/add-category','CategoryController@createCategory')->name('createCategory');

// brand
    Route::get('/brand','BrandController@show')->name('brand');
    Route::get('/add-brand','BrandController@create')->name('addBrand');
    Route::post('/add-brand','BrandController@createBrand')->name('createBrand');
    Route::get('/delete-brand/{id}','BrandController@destroy')->name('deleteBrand');

// blog
    Route::get('/blog','BlogController@list')->name('blog');
    Route::get('/add-blog','BlogController@addBlog')->name('addBlog');
    Route::post('/add-blog','BlogController@createBlog')->name('createBlog');
    Route::get('/edit-blog/{id}','BlogController@editBlog')->name('editBlog');
    Route::post('/edit-blog/{id}','BlogController@updateBlog')->name('updateBlog');
    Route::get('/delete-blog/{id}','BlogController@deleteBlog')->name('deleteBlog');


    Route::get('/form-basic','FormBasicController@formBasic')->name('formBasic');
    Route::get('/table-basic','TableBasicController@tableBasic')->name('tableBasic');
    Route::get('/icon-material','IconMaterialController@iconMaterial')->name('iconMaterial');
    Route::get('/blank','StarterKitController@starterKit')->name('blank');
    Route::get('/error-404','Error404Controller@error404')->name('error404');
});
