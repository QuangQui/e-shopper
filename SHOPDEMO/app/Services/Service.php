<?php

namespace App\Services;

use App\Models\User;
use Mail;
use DB;

class Service 
{
    /**
     * postRegister
     * 
     * @param $data
     * 
     * @return bool
     */
    public function postRegister($data)
    {   

        // dd($data['email']);
        // dd($data);
        
        try {
            $dataUser = [
                'name'       => $data['name'],
                'email'      => $data['email'],
                'phone'      => $data['phone'],
                'password'   => bcrypt($data['password']),
                'level'      => User::LEVEL_DEFAULT,
            ];

            if(!empty($data->avatar))
            {
                $dataUser['avatar'] = $file->getClientOriginalName();
            }

            return User::create($dataUser);
        } catch (\Throwable $th) {
            \Log::error($th->getMessage());
            throw $th;
        }
    }

    public function sendMailOrderComplete($user)
    {
        $emailBody = '';
        $fromName = 'tqqui.it@gmail.com';
        $fromEmail = 'tqqui.it@gmail.com';
        $devUser = 'tqqui.it@gmail.com';
        $emailLid = $user->email;
        // $emailLid = 'Sinh_1851030156@dau.edu.vn';
        $mailTitle = 'tqqui.it@gmail.com';
        $replyToEmail = 'tqqui.it@gmail.com';
        $emailBody .= '<p class="pTag">Chào '.$user->name.',</p><br><p class="pTag"> Don hang cua ban da dat thanh cong! </p>';
        $emailcontent = array(
            'emailBody' =>$emailBody
        );
        $result = Mail::send(['html'=> 'frontend.sendemail.mail'], $emailcontent, function ($message) use($fromName, $fromEmail,
                            $devUser, $emailLid, $mailTitle, $replyToEmail) 
        {
            $message->from($fromEmail, $fromName);
            $message->to($emailLid);
            $message->cc($devUser);
            $message->replyTo($replyToEmail);
            $message->subject($mailTitle);
        });
    }
}
