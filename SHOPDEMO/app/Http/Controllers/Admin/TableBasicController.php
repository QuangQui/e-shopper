<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TableBasicController extends Controller
{
    public function tableBasic()
    {
        return view('admin.table-basic.table-basic');
    }
}
