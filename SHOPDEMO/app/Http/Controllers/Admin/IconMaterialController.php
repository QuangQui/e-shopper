<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IconMaterialController extends Controller
{
    public function iconMaterial()
    {
        return view('admin.icon-material.icon-material');
    }
}
