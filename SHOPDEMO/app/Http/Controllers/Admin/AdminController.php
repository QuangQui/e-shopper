<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Models\Blog;
class AdminController extends Controller
{
    public function index()
    {
        return view('admin.layout.index');
    }
    public function logout()
    {
        return view('Auth.login');
    }
}
