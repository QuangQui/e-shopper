<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function profile()
    {

        $data = Country::all()->toArray();
        // dd($data);
        return view('admin.user.user',compact('data'));
    }
    public function update(UpdateProfileRequest $request)
    {
        $userld = Auth::id();
        $user = User::findOrFail($userld);
        $data = $request->all();
        $file = $request->avatar;
        // dd($data);
        if(!empty($file))
        {
            $data['avatar'] = $file->getClientOriginalName();
        }
        if($data['password'])
        {
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
    
        if($user->update($data))
        {
            if(!empty($file))
            {
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Updata profile success'));
        }else{
            return redirect()->back()->withError('Update profile error');
        }

    }
}