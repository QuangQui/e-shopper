<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Error404Controller extends Controller
{
    public function error404()
    {
        return view('admin.error-404.error-404');
    }
}
