<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Http\Requests\BlogRequest;
class BlogController extends Controller
{
    public function list()
    {
        // $data = Blog::all()->toArray();
        $blogs = Blog::simplePaginate(3);
        // dd($data);
        return view('admin.blog.blog',compact('blogs'));
    }

    public function addBlog()
    {
        return view('admin.blog.add');
    }

    public function createBlog(BlogRequest $request)
    {
        $data = new Blog;
        $data->title = $request->title;
        $file = $request->image;
        if(!empty($file))
        {
            $data->image = $file->getClientOriginalName();
        }
        $data->description = $request->description;
        $data->content = $request->content;
        if($data->save())
        {
            if(!empty($file))
            {
                $file->move('upload/blog/image', $file->getClientOriginalName());
            }
            return redirect()->route('admin.blog')->with('success',_('Add blog success!'));
        }
        return redirect()->back()->withError('success',_('Add blog error!'));
    }

    public function editBlog($id)
    {
        $value = Blog::find($id)->toArray();
        // dd($value);
        return view('admin.blog.edit',compact('value'));
    }

    public function updateBlog(BlogRequest $request, $id)
    {
        $blog = Blog::find($id);
        $data = $request->all();
        $file = $request->image;
        if(!empty($file))
        {
            $data['image'] = $file->getClientOriginalName();
        }
        if($blog->update($data)) 
        {
            if(!empty($file))
            {
                $file->move('upload/blog/image', $file->getClientOriginalName());
            }
            return redirect()->route('admin.blog')->with('success',_('Add blog success!'));
        }
        return redirect()->back()->withError('success',_('Add blog error!'));
    }

    public function deleteBlog($id)
    {
        $data = Blog::find($id);
        if($data->delete())
        {
            return redirect()->route('admin.blog')->with('success',_('Delete blog success!'));
        }
        return redirect()->back()->withError('success',_('Delete blog error!'));
    }
}
