<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateRequest;
use App\Models\Country; 
use DB;
class CountryController extends Controller
{
    public function country()
    {
        $data = Country::all()->toArray();
        // dd($data);
        return view('admin.country.country', compact('data'));
    }

    public function addCountry()
    {
        return view('admin.country.add');
    }

    public function createCountry(CreateRequest $request)
    {
        $data = new Country;
        $data->name = $request->name;
        if($data->save()){
            return redirect()->route('admin.country')->with('success',_('Add country success!'));
        }
        return redirect()->back()->withError('success',_('Add country error!'));
    }

    public function deleteCountry($id)
    {
        // $country = Auth::id();
        // dd($country);
        $data = Country::find($id);
        // dd($data);
        if($data->delete())
        {
            return redirect()->route('admin.country')->with('success',_('Delete country success!'));
        }
        return redirect()->back()->withError('success',_('Delete country error!'));
    }
}
