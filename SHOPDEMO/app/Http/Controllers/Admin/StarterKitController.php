<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StarterKitController extends Controller
{
    public function starterKit()
    {
        return view('admin.starter-kit.starter-kit');
    }
}
