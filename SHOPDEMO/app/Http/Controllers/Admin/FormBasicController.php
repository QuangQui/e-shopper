<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FormBasicController extends Controller
{
    public function formBasic()
    {
        return view('admin.form-basic.form-basic');
    }
}
