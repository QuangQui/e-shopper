<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Blog::paginate(3);
        return view('frontend.blog.blog',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showBlogSingle($id)
    {
        // dd($id);
        $blog = Blog::find($id);
         // get previous user id
        $previous = Blog::where('id', '<', $blog->id)->max('id');

        // get next user id
        $next = Blog::where('id', '>', $blog->id)->min('id');
        $comments = Comments::where('blog_id',$id)->get();
        $rating = Rate::where('blog_id',$id)->avg('point');
        $rating = round($rating);
        // dd($sumPoint);
        // tong so diem / so lan danh gia cua bai do , thi tron: round php
        // dd($comments);
        return view('frontend.blog.blogSingle',compact('blog', 'previous','next','comments','rating'));
    }

    //Rate
    public function blogRate()
    {
        $blog_id = $_POST['idBlog'];
        $point = $_POST['point'];
        $rate = Rate::create([
            'blog_id' => $blog_id,
            'user_id' =>Auth::user()->id,
            'point' => $point
        ]);
        return redirect()->route('frontend.blogSingle',['id'=>$blog_id]);
    }

    public function cmtBlogSingle(Request $request)
    {
        dd($request->all());
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
