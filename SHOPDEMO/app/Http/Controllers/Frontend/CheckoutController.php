<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MemberRegisterRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\History;
use Mail;
use App\Services\Service;
class CheckoutController extends Controller
{
    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.checkout.checkout');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total = 0;
        if(session()->has('cart')){
            $carts = session()->get('cart');
            foreach ($carts as $key => $value) {
                $total += $value['price'] * $value['qty'];
            }
        }
        if(Auth::check()){
            $user = Auth::user();
            
            //send email for user
            $this->service->sendMailOrderComplete($user);

            //save history
            History::create([
                'email' => $user->email,
                'phone' => $user->phone,
                'name'  => $user->name,
                'id_user' => $user->id,
                'price' => "$".$total
            ]);
        }else{
            //register
            $this->service->postRegister($request->all());

            //send email
            $user = $request;
            $this->service->sendMailOrderComplete($user);

            // save history
            $user = User::latest()->limit(1)->get();
            foreach ($user as $key => $value) {
                History::create([
                'email' => $value->email,
                'phone' => $value->phone,
                'name'  => $value->name,
                'id_user' => $value->id,
                'price' => "$".$total
                ]);
            }
        }
        return view('frontend.checkout.success');
        // return view('frontend.sendemail.mail',compact('emailBody'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('frontend.checkout.success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
