<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('cart')){
            $carts = session()->get('cart');
            return view('frontend.cart.cart', compact('carts'));
        }
        return view('frontend.cart.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // session()->forget('cart');
        $id = $_POST['idProduct'];
        $product = Product::find($id)->toArray();
        $cart = session()->get('cart');
        // dd($product);
        if(isset($cart[$id])){
            $cart[$id]['qty'] = $cart[$id]['qty'] += 1;
        }else {
            $cart[$id] = [
                'img' => current(json_decode($product['images'])),
                'price' => $product['price'],
                'name' => $product['name'],
                'qty' => 1
            ];
        }
        session()->put('cart', $cart);
        $countCart = count(session()->get('cart'));
        return response()->json([
            'code' => 200,
            'countCart' => $countCart,
            'message' => 'success'
        ],200);

        // echo "<pre>";
        // print_r(session()->get('cart'));
    }

    public function cartQtyUp()
    {
        // session()->forget('cart');
        $idProduct = $_POST['valID'];
        if(session()->has('cart'))
        {
            $cart = session()->get('cart');
            $total = 0;
            foreach($cart as $key => $value){
                if($key == $idProduct){
                    $cart[$key]['qty'] += 1;
                }
            }
            session()->put('cart',$cart);
            foreach($cart as $key => $value){
                $total += $value['price'] * $value['qty'];
            }
            return response()->json([
                'total' => $total
            ]);
        }else{
            echo "Session does not exist! ";
        }
    }

    public function cartQtyDown()
    {
        $idProduct = $_POST['valID'];
        if(session()->has('cart')){
            $cart = session()->get('cart');
            $total = 0;
            foreach ($cart as $key => $value) {
                if($key == $idProduct){
                    $cart[$key]['qty'] -= 1;
                }
                if($cart[$key]['qty'] < 1){
                    unset($cart[$key]);
                }
            }
            session()->put('cart', $cart);
            foreach($cart as $key => $value){
                $total -= $value['price'] * $value['qty'];
            }
            return response()->json([
                'total' => $total
            ]);
        }else{
            echo "Session does not exist! ";
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $idProduct = $_GET['valID'];
        if(session()->has('cart')){
            $cart = session()->get('cart');
            foreach ($cart as $key => $value) {
                if($key == $idProduct){
                    unset($cart[$key]);
                }
            }
            session()->put('cart', $cart);
        }else{
            echo "Session does not exist! ";
        }
    }
}
