<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use Image;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $product = Product::find($id);
        $brand = Brand::all()->toArray();
        $getArrImage = json_decode($product['images'],true);
        // dd($getArrImage);
        return view('frontend.product.product-details',compact('product','getArrImage','brand'));
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorys = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        // dd($product);
        return view('frontend.product.add',compact('categorys', 'brands'));
    }

    // createProduct
    public function createProduct(ProductRequest $request)
    {
        $product = new Product;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->id_category = $request->category;
        $product->id_brand = $request->brand;
        $product->status = $request->status;
        $product->sale = $request->sale;
        $product->company = $request->company;
        $product->detail = $request->detail;
        $data = [];

        $time = strtotime(date('Y-m-d H:i:s'));
        // echo $time;
        //kiem tra file
        if($request->hasfile('avatar'))
        {
            foreach($request->file('avatar') as $image)
            {
                if(!is_dir('upload/product/'. Auth::user()->id .'/')){
                    $image->move('upload/product/'. Auth::user()->id .'/', '');
                }
                $name   = $time."_".$image->getClientOriginalName();
                $name_2 = "hinh85_".$time."_".$image->getClientOriginalName();
                $name_3 = "hinh329_".$time."_".$image->getClientOriginalName();
                
                $path = public_path('upload/product/'. Auth::user()->id .'/' . $name);
                $path2 = public_path('upload/product/'. Auth::user()->id .'/' . $name_2);
                $path3 = public_path('upload/product/'. Auth::user()->id .'/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                Image::make($image->getRealPath())->resize(329, 380)->save($path3);

                $data[] = $name;
            }
        }
        $product->images = json_encode($data);
        if($product->save())
        {
            return redirect()->route('frontend.addProduct')->with('success',_('Add product success!'));
        }
        return redirect()->back()->withError('success',_('Add blog error!'));

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // show shop
    public function showShop(Request $request)
    {
        $products = Product::query();
        $categorys = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        $nameProduct = $request->nameProduct;
        if(!empty($nameProduct)){   
            $products->where('name', 'like', "%$nameProduct%");
        }
        $products = $products->paginate(9); 
        return view('frontend.shop.shop', compact('products','categorys', 'brands'));
        // dd($products);
    }

    // searchAdvanced
    public function store(Request $request)
    {
        if($request->ajax()){
            $nameProduct =  $_POST['nameProduct'];
            $price =  $_POST['price'];
            $category = $_POST['category'];
            $brand = $_POST['brand'];
            $status = $_POST['status'];
            $products = Product::query();

            if(!empty($nameProduct)){   
                $products->where('name', 'like', "%$nameProduct%");
            }  

            if(!empty($price)){
                $price = explode("-",$price);
                $products ->whereBetween('price', [$price[0],$price[1]]);
            }
            
            if(!empty($category)){
                $products->where('id_category', $category );
            }

            if(!empty($brand)){
                $products->where('id_brand', $brand );
            }

            if(!empty($status)){
                $products->where('status', $status );
            }

            $products = $products->get();
            return view('frontend.shop.products', compact('products'));
        }
    }

    // priceRange
    public function priceRange(Request $request)
    {
        if($request->ajax()){
            $minPrice = $_GET['min_price'];
            $maxPrice = $_GET['max_price'];
            $products = Product::query()->whereBetween('price', [$minPrice,$maxPrice])
                                        ->orderBy('price')
                                        ->get();
            return view('frontend.shop.products', compact('products'));
        }   
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $listProduct = Product::simplePaginate(6);
        // dd($product);
        return view('frontend.product.product',compact('listProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $getArrImage = json_decode($product['images'],true);
        $categorys = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        return view('frontend.product.edit', compact('product','categorys','brands','getArrImage'));
    }

    public function updateProduct(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->id_category = $request->category;
        $product->id_brand = $request->brand;
        $product->status = $request->status;
        $product->sale = $request->sale;
        $product->company = $request->company;
        $product->detail = $request->detail;
        // chuyen doi j
        $getArrImage = json_decode($product['images'],true);
        $data = [];
        $time = strtotime(date('Y-m-d H:i:s'));
        $file = $request->images;
        // kiem tra file ton tai ko
        if(!empty($file)){
            foreach($getArrImage as $key => $image){
                if(in_array($image,$file)){
                    unset($getArrImage[$key]);
                }
            }
        }
        // print_r($getArrImage);
        foreach($getArrImage as $image){
            $data[] = $image;
        }
        // kiem tra file
        if($request->hasfile('avatar'))
        {   
            // dd($request->avatar);
            foreach($request->file('avatar') as $image)
            {
                if(!is_dir('upload/product/'. Auth::user()->id .'/')){
                    $image->move('upload/product/'. Auth::user()->id .'/', '');
                }
                $name   = $time."_".$image->getClientOriginalName();
                $name_2 = "hinh85_".$time."_".$image->getClientOriginalName();
                $name_3 = "hinh329_".$time."_".$image->getClientOriginalName();
                
                $path = public_path('upload/product/'. Auth::user()->id .'/' . $name);
                $path2 = public_path('upload/product/'. Auth::user()->id .'/' . $name_2);
                $path3 = public_path('upload/product/'. Auth::user()->id .'/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                Image::make($image->getRealPath())->resize(329, 380)->save($path3);

                $data[] = $name;
            }
        }
        // dd($data);
        $countImg = count($data);
        if($countImg <= 3){
            $product->images = json_encode($data);
            if($product->update())
            {
                return redirect()->back()->with('success',_('Update product success!'));
            }
        }else{
            return redirect()->back()->with('success',_('Images in array must be < 3!'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product->delete()){
            return redirect()->back()->with('success',_('Delete product success!'));
        }
        return redirect()->back()->withError('success',_('Delete product error!'));
    }
}



