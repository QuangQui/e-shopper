<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\MemberRegisterRequest;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Services\Service;

class MemberController extends Controller
{

    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.member.login');
    }

    public function register()
    {
        return view('frontend.member.register');
        
    }

    public function postRegister(MemberRegisterRequest $request)
    {
        
        try {
            $this->service->postRegister($request->all());

            return redirect()->back()->with('success',_('Register success.'));
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('success',_('Register error.'));
        }
    }

    public function login()
    {
        return view('frontend.member.login');
        
    }

    public function postLogin(MemberLoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password'=> $request->password
        ];

        $remember = false;

        if($request->remember_me){
            $remember = true;
        }
        if ($this->doLogin($login, $remember)) {
            return redirect('/frontend/home');
        }else{
            return redirect()->back()->with('success',_('Email or password is not correct.'));
        }
    }
    /**
     * Do login
     *
     * @param $attempt
     * @param $remember
     * @return bool
     */
    protected function doLogin($attempt, $remember)
    {
        
        if (Auth::attempt($attempt, $remember)) {
            return true;
        } else {
            return false;
        }
    }
    public function logout()
    {
        if(Auth::check()){
            Auth::logout();
        }
        return redirect()->route('frontend.login');
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if(Auth::check()){
            return view('frontend.member.account');
        }
        return view('frontend.member.login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        $userID = Auth::id();
        $user = User::find($userID);
        // dd($user);
        $data = $request->all();
        $file = $request->avatar;

        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }
        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }

        if($user->update($data)){
            if(!empty($file))
            {
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Updata profile success'));
        }else{
            return redirect()->back()->withError('Update profile error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
