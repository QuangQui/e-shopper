<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function RegisterUser(Request $request)
    {
        $data = new User;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->level = 0;
        $file = $request->avatar;
        // var_dump($file);
        if(!empty($file))
        {
            $data->avatar = $file->getClientOriginalName();
        }
        if($data->save()){
            if(!empty($file))
            {
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Register success.'));
        }
        return redirect()->back()->with('success',_('Register error.'));
    }
}
