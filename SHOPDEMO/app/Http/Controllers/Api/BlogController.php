<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Http\Requests\BlogRequest;

class BlogController extends Controller
{
    public function list()
    {
        
        
        $blogs = Blog::simplePaginate(3);
        
        // return view("xxx", compact('getBlogListComment'))

        return response()->json([
            'blog' => $blogs
        ]);
        
    }
}
