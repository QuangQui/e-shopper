<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MemberLoginRequest;
use App\Models\User;
class MemberController extends Controller
{
    public function login(MemberLoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];
        $remember = false;
        if ($request->remember_me) {
            $remember = true;
        }
       

        if ($this->doLogin($login, $remember)) {
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 

            return response()->json([
                    'response' => 'success',
                    'success' => $success, 
                    'Auth' => Auth::user()
                ], 
                $this->successStatus
            ); 

            // $token = Auth::attempt($login);
            // return response()->json([
            //     'response' => 'success',
            //     'result' => [
            //         'token' => $token,
            //     ],
            //     'Auth' => Auth::user()
            // ], JsonResponse::HTTP_OK);

        } else {
            return response()->json([
                    'response' => 'error',
                    'errors' => ['errors' => 'invalid email or password'],
                ],
                $this->successStatus); 
            // return response()->json([
            //     'response' => 'error',
            //     'errors' => ['errors' => 'invalid email or password'],
            // ], JsonResponse::HTTP_OK);
        }
    }
}
