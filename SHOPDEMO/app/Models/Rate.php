<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rate';
    public $timestamps = false;
    protected $fillable =[
        'blog_id',
        'user_id',
        'point'
    ];
}
