<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = [
        'name',
        'price',
        'id_category',
        'id_brand',
        'status',
        'sale',
        'company',
        'images',
        'detail'
    ];
}
