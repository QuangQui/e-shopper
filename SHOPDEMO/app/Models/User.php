<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    const LEVEL_DEFAULT = 0;
    // public $timestamps = false;
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'avatar',
        'country',
        'level'
    ];
}
