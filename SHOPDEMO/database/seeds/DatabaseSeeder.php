<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BlogSeeder::class);
    }
}
class BlogSeeder extends Seeder 
{
    public function run()
    {
        $data = [
            'title'=>'Video highlight MU - MC',
            'image'=>'abc.png',
            'description'=>'Trước tình hình dịch bệnh diễn biến hết sức phức tạp, khó lường, trên địa bàn Thành phố Hà Nội đã xuất hiện chùm ca bệnh mới chưa xác định nguồn lây nhiễm tại các khu vực chung cư tập trung đông dân, trụ sở cơ quan, doanh nghiệp. ',
            'content'=>'Bong da'
        ];
        DB::table('blog')->insert($data);
    }
}
