@extends('admin.layout.index')
@section('title','Country')
@section('content')
<div>
<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ __('Category') }}</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/admin/dashboard') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ __('Category') }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($category as $key =>$value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value['name']}}</td>
                                    <td>
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.deleteCategory',['id'=>$value['id']])}}" aria-expanded="false"
                                        onclick="return confirm('Ban co muon xoa khong?')">
                                            <i class="mdi mdi-delete"></i>
                                        </a>
                                    </td>
                                </tr>  
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div style="float: right;">
                    {{ $category->links() }}
                </div>
            </div>
            <div class="col-12">
                        <div class="card">
                            <div class="table-responsive">
                                	<a href="{{route('admin.addCategory')}}"><button style="display: block ;" type="submit" class="btn btn-success">Add Category</button></a>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>
@endsection