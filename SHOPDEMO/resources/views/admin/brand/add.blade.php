@extends('admin.layout.index')
@section('title','Add-Brand')
@section('content')
<div>
<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ __('Create Brand') }}</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/admin/dashboard') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ __('Add Brand') }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
             @endif

            <div class="col-12">
                <div class="card">
                    <div>
                    <p style="margin: 10px 10px;">Name Brand <span style="color: red;">(*)</span></p>
                    <form action="" method="post">
                        @csrf
                        <input style="margin: 0px 10px; width: 220px;" type="text" name="name" id="">
                        <button style="display: block;margin: 20px 10px;" type="submit" class="btn btn-success">Create Brand</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection