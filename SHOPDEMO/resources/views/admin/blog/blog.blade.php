@extends('admin.layout.index')
@section('title','Blog')
@section('content')
<div>
<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ __('Blog') }}</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/admin/dashboard') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ __('Blog') }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blogs as $key => $blog)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$blog['title']}}</td>
                                    <td>
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" style="display: block; padding-bottom: 15px;"
                                         href="{{route('admin.editBlog',['id' =>$blog['id']])}}" aria-expanded="false">
                                            <i class="mdi  mdi-account-edit "></i>Edit
                                        </a>
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.deleteBlog',['id' =>$blog['id']])}}" aria-expanded="false"
                                        onclick="return confirm('Ban co muon xoa khong?')">
                                            <i class="mdi mdi-delete"></i>Delete
                                        </a>
                                    </td>
                                </tr>  
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                            <a href="{{route('admin.addBlog')}}"><button style="display: block ;" type="submit" class="btn btn-success">Add Country</button></a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div style="float: right;">
                    {{ $blogs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection