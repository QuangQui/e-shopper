@extends('frontend.layout.index')
@section('title','Shop')
@section('content')
<style>
    select{
        width: 166px;
        height: 33px;
    }
    .nameProduct {
        margin-left: 15px;
        width: 155px;
        border: none;
        background: #F0F0E9;
        height: 33px;
        padding-left: 10px;
    }
</style>
<div class="features_items"><!--features_items-->
    <h2 class="title text-center">Features Items</h2>
    <div class="form_search ">
        <form action="{{ route('frontend.searchProduct')}}">
            @csrf
            <input class="nameProduct" type="text" name="nameProduct" placeholder="Name product"/>
            <select class="price" name="price" id="price">
                <option value="" selected>Choose price</option>
                <option value="50-500">$50-$500</option>
                <option value="500-1000">$500-$1000</option>
                <option value="1000-5000">$1000-$5000</option>
            </select>
            <select id="category" name="category">
                <option value="" selected>Choose category</option>
                    @foreach($categorys as $category)
                            <option value="{{$category['id']}}"
                            >{{$category['name']}}
                            </option>
                    @endforeach
            </select>
            <select id="brand" name="brand">
                <option value="" selected>Choose brand</option>
                    @foreach($brands as $brand)
                            <option value="{{$brand['id']}}"
                            >{{$brand['name']}}
                            </option>
                    @endforeach
            </select>
            <select class="status" id="status" name="status">
                <option value="" selected>Status</option>
                <option value="0">New</option>
                <option value="1">Sale</option>
            </select>
            <button id="btn_search" style="border: none;background: #FE980F;height: 33px;color: #ffffff;margin-left: 15px;margin-top: 10px;margin-bottom: 10px;width: 90px;">Search</button>
        </form>
    </div>
    <div id="updateDiv">
        @foreach($products as $product)
        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                        <div class="productinfo text-center">
                            @if(!auth()->check())
                                <img src="../../upload/product/16/{{ current(json_decode($product['images'])) }}" alt="" />
                            @else
                                <img src="../../upload/product/{{ Auth::user()->id}}/{{ current(json_decode($product['images'])) }}" alt="" />
                            @endif
                            <h2>${{ $product['price'] }}</h2>
                            <p>{{ $product['name'] }}</p>
                            <a id="{{ $product['id'] }}" href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <h2>{{ $product['price'] }}</h2>
                                <p>{{ $product['name'] }}</p>
                                <a id="{{ $product['id'] }}"  href="" class="btn btn-default add-to-cart btn_addToCart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                <a href="{{route('frontend.detailProduct',['id'=>$product['id']])}}" class="btn btn-default add-to-cart">Detail</a>
                            </div>
                        </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                        <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    
</div>
<ul class="pagination">
    {{ $products->links() }}
</ul>
<script>
    $(document).ready(function(){
        $('#btn_search').click(function(){
            var nameProduct = $(this).closest('div.form_search').find('input.nameProduct').val();
            var price = $(this).closest('div.form_search').find('select#price').val();
            var category = $(this).closest('div.form_search').find('select#category').val();
            var brand = $(this).closest('div.form_search').find('select#brand').val();
            var status = $(this).closest('div.form_search').find('select#status').val();
            // console.log(price)
            $.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    }
				    });
            $.ajax({
                type: "post",
                url: "{{ route('frontend.searchProduct') }}",
                data: {nameProduct:nameProduct,price:price,category:category,brand:brand,status:status},
                success:function(data){
                    $('#updateDiv').html(data);
                }
            })
            return false;
        });
    })
</script>
@endsection

@section('slider')
<section id="advertisement">
    <div class="container">
        <img src="../images/shop/advertisement.jpg" alt="" />
    </div>
</section>
@endsection