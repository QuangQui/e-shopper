
@foreach($products as $product)
<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
                <div class="productinfo text-center">
                    @if(!auth()->check())
                        <img src="../../upload/product/16/{{ current(json_decode($product['images'])) }}" alt="" />
                    @else
                        <img src="../../upload/product/{{ Auth::user()->id}}/{{ current(json_decode($product['images'])) }}" alt="" />
                    @endif
                    <h2>${{ $product['price'] }}</h2>
                    <p>{{ $product['name'] }}</p>
                    <a id="{{ $product['id'] }}" href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
                <div class="product-overlay">
                    <div class="overlay-content">
                        <h2>{{ $product['price'] }}</h2>
                        <p>{{ $product['name'] }}</p>
                        <a id="{{ $product['id'] }}"  href="" class="btn btn-default add-to-cart btn_addToCart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        <a href="{{route('frontend.detailProduct',['id'=>$product['id']])}}" class="btn btn-default add-to-cart">Detail</a>
                    </div>
                </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
            </ul>
        </div>
    </div>
</div>
@endforeach
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('.btn_addToCart').click(function(){
            var idProduct = $(this).attr('id');
            // alert(idProduct);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "{{ route('frontend.addToCart') }}",
                data:{
                    idProduct:idProduct
                },
                success:function(data){
                        $('span.countCart').text(data.countCart);
                        if(data.code == 200){
                            alert('Add to cart success!')
                        }
                }
            });
            return false;
        })
    })
</script>