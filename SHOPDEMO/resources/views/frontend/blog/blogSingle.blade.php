@extends('frontend.layout.index')
@section('title','Blog-single')
@section('content')
<div>
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        
        <div class="single-blog-post">
            <h3>{{$blog->title }}</h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> Mac Doe</li>
                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>
                <span>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                </span>
            </div>
            {!! $blog->content  !!}
            <div class="pager-area">
                <div style="float: right;">
                    <nav>
                        <ul class="pagination">
                            <li class="page-item">
                                @if($previous)
                                    <a href="{{ URL::to( 'Frontend/blog-single/' . $previous ) }}">« Previous</a>
                                @endif
                            </li>
                            <li class="page-item">
                                @if($next)
                                    <a href="{{ URL::to( 'Frontend/blog-single/' . $next ) }}">Next »</a>
                                @endif
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div><!--/blog-post-area-->
    <div class="rating-area">
        <ul class="ratings">
            {{-- {{ dd($rate) }} --}}
            <li class="rate-this">Rate this item:</li>
            <li>
                {{-- {{dd($rating)}} --}}
                <div class="rate">
                    <div class="vote">
                        <?php 
                            for($i=1; $i < 6 ; $i++) {
                            if($i <= $rating){
                                $color = ' ratings_over';
                            }else{
                                $color = '';
                            }
                        ?>
                        <div class="star_{{$i}} ratings_stars {{ $color }}"><input value="{{$i}}" type="hidden"></div>
                        <?php } ?>
                        <span class="rate-np">{{ $rating }}</span>
                    </div> 
                </div>
            </li>
        </ul>
        <ul class="tag">
            <li>TAG:</li>
            <li><a class="color" href="">Pink <span>/</span></a></li>
            <li><a class="color" href="">T-Shirt <span>/</span></a></li>
            <li><a class="color" href="">Girls</a></li>
        </ul>
    </div><!--/rating-area-->

    <div class="socials-share">
        <a href=""><img src="../../images/blog/socials.png" alt=""></a>
    </div><!--/socials-share-->

    <div class="media commnets">
        <a class="pull-left" href="#">
            <img class="media-object" src="../../images/blog/man-one.jpg" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Annie Davis</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            <div class="blog-socials">
                <ul>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
                <a class="btn btn-primary" href="">Other Posts</a>
            </div>
        </div>
    </div><!--Comments-->
    <div class="response-area">
        <h2>3 RESPONSES</h2>
        <ul class="media-list">
            {{-- comments --}}
            @foreach($comments as $comment) 
                @if($comment->level == 0)
                    <li class="media" id="{{$comment->id}}">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="../../images/blog/{{$comment->avatar}}" alt="">
                        </a>
                        <div class="media-body">
                            <ul class="sinlge-post-meta">
                                <li><i class="fa fa-user"></i>{{$comment->name}}</li>
                                <li><i class="fa fa-clock-o"></i> {{$comment->created_at}}</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <p>{{$comment->content}}</p>
                            <a class="btn btn-primary" id="" href="#form_cmt">Replay</a>
                        </div>
                    </li>  
                @endif
                {{-- reply --}}
                @foreach($comments as $reply)
                    @if($reply->level == $comment->id)
                        {{-- {{dd($reply)}} --}}
                        <li class="media second-media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="../../images/blog/{{$reply->avatar}}" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>{{$reply->name}}</li>
                                    <li><i class="fa fa-clock-o"></i> {{$reply->created_at}}</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>{{$reply->content}}</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                    @endif
                @endforeach
            @endforeach
        </ul>					
    </div><!--/Response-area-->
    <div class="replay-box">
        <div class="row">
            <div class="col-sm-4">
                <h2>Leave a replay</h2>
                <form>
                    <div class="blank-arrow">
                        <label>Your Name</label>
                    </div>
                    <span>*</span>
                    <input type="text" placeholder="write your name...">
                    <div class="blank-arrow">
                        <label>Email Address</label>
                    </div>
                    <span>*</span>
                    <input type="email" placeholder="your email address...">
                    <div class="blank-arrow">
                        <label>Web Site</label>
                    </div>
                    <input type="email" placeholder="current city...">
                </form>
            </div>
            <div class="col-sm-8">
                <form action="{{ route('frontend.cmtBlog',['id' => $blog->id]) }}" method="post" id="form_cmt">
                    @csrf
                    <div class="text-area">
                        <div class="blank-arrow">
                            <label>Your Name</label>
                        </div>
                        <span>*</span>
                        <textarea id="textarea" class="form_cmt" name="comment" rows="11"></textarea>
                        <input type="hidden" name="input" class="idBlog" value="0">
                        <button class="btn btn-primary btn_cmt" id="submit-comment">Post comment</button>
                    </div>
                </form>
               
                <script>
                    $(document).ready(function(){
                        //check login
                        var checkLogin = '{{ auth()->check() }}';
                        
                        //comment
                        $("#submit-comment").click(function(){
                            if(!checkLogin){
                                alert('Vui lòng login!');
                                return false;
                            } else {
                                $this.submit();
                            }
                        })

                        $('.btn-primary').click(function(){
                            if(!checkLogin){
                                alert('Vui long login!');
                                return false;
                            }else{
                                var valId = $(this).closest('li.media').attr('id');
                                $(this).attr('id',valId);
                                var getId = $(this).attr('id');
                                var idBlog = $('input.idBlog').attr('value',getId);
                                $(this).load(attr('href'));
                            }
                            return false;
                        })

                        //vote
                        $('.ratings_stars').hover(
                            // Handles the mouseover
                            function() {
                                $(this).prevAll().andSelf().addClass('ratings_hover');
                                // $(this).nextAll().removeClass('ratings_vote'); 
                            },
                            function() {
                                $(this).prevAll().andSelf().removeClass('ratings_hover');
                                // set_votes($(this).parent());
                            }
                        );

                        $('.ratings_stars').click(function(){
                            // alert('ok');
                            if(!checkLogin){
                                alert('Vui long login!');
                            }else {
                                var values =  $(this).find("input").val();
                                var idBlog ='{{ $blog->id }}';
                                var _token = $('input[name="_token"]').val();
                                
                                // $(this).closest('div.vote').find('span.rate-np').text(Values);
                                // alert(valRate);
                                if ($(this).hasClass('ratings_over')) {
                                    $('.ratings_stars').removeClass('ratings_over');
                                    $(this).prevAll().andSelf().addClass('ratings_over');
                                } else {
                                    $(this).prevAll().andSelf().addClass('ratings_over');
                                }
                                $.ajax({
                                    type: "POST",
                                    url: "{{ route('frontend.blogRate') }}",
                                    data:{
                                        point:values,
                                        _token:_token,
                                        idBlog:idBlog
                                        },
                                    success:function(data){
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    
                    })
                </script>
            </div>
        </div>
    </div><!--/Repaly Box-->
</div>
@endsection