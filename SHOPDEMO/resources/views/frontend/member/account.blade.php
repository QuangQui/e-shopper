
@extends('frontend.layout.index-cart')
@section('title','Account')
@section('content')
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="account.php">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<a href="{{ Route('frontend.showAccount') }}">Account</a>
										</a>
									</h4>
								</div>								
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<a href="{{ Route('frontend.showProduct') }}">My Product</a>
										</a>
									</h4>
								</div>
							</div>
						</div><!--/category-products-->
					
						<!--/brands_products-->
						
						
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
                    <div class="row" style="margin-bottom: 100px;">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="signup-form"><!--sign up form-->
                                <h2>User Update!</h2>
                                @if(session('success'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                        <h4><i class="icon fa fa-check"></i>Thong bao!</h4>
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="" method ="post" enctype="multipart/form-data" style="width: 400px;">
                                    @csrf
                                    <input type="text" name="name" value="{{ Auth::User()->name }}" placeholder="Name"/>
                                    <input type="email" name="email" value="{{ Auth::User()->email }}" placeholder="Email Address"/>
                                    <input type="password" name="password" value="{{ Auth::User()->password }}" placeholder="Password"/>
                                     <input style="padding-top: 10px;" type="file" name="avatar" id="avatar">
                                    <button type="submit" class="btn btn-default">Update User</button>
                                </form>
                            </div><!--/sign up form-->
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
				</div>
			</div>
		</div>
	</section>
@endsection
	

