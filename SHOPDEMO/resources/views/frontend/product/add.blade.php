
@extends('frontend.layout.index-cart')
@section('title','Account')
@section('content')
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>My Product</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="account.php">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<a href="{{ Route('frontend.showAccount') }}">Account</a>
										</a>
									</h4>
								</div>								
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<a href="{{ Route('frontend.showProduct') }}">My Product</a>
										</a>
									</h4>
								</div>
							</div>
						</div><!--/category-products-->
					
						<!--/brands_products-->
						
						
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
                    <div class="row" style="margin-bottom: 100px;">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="signup-form"><!--sign up form-->
                                <h2>Create product!</h2>
                                @if(session('success'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                        <h4><i class="icon fa fa-check"></i>Thong bao!</h4>
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="" method ="post" enctype="multipart/form-data" style="width: 400px;">
                                    @csrf
                                    {{ csrf_field() }}
                                    <input type="text" name="name" value="" placeholder="Name"/>
                                    <input type="text" name="price" value="" placeholder="Price"/>
                                    <select id="category" name="category">
                                        <option value="category" selected>Please choose category</option>
                                            @foreach($categorys as $category)
                                                    <option value="{{$category['id']}}"
                                                    >{{$category['name']}}
                                                    </option>
                                            @endforeach
                                    </select>
                                    <select id="brand" name="brand">
                                        <option value="brand" selected>Please choose brand</option>
                                            @foreach($brands as $brand)
                                                    <option value="{{$brand['id']}}"
                                                    >{{$brand['name']}}
                                                    </option>
                                            @endforeach
                                    </select>
                                    <select class="status" id="status" name="status">
                                        <option value="0" selected>New</option>
                                        <option value="1">Sale</option>
                                    </select>
                                    <input id="sale" class="hide" type="number" name="sale" value="" placeholder="%"/>
                                    <input type="text" name="company" value="" placeholder="Company profile"/>
                                    <input style="padding-top: 10px;" type="file" name="avatar[]" id="avatar" multiple>
                                    <textarea name="detail" id="detail" cols="30" rows="10" placeholder="Detail"></textarea>
                                    <button type="submit" class="btn btn-default">Save</button>
                                </form>
                            </div><!--/sign up form-->
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
				</div>
			</div>
		</div>
	</section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#status').click(function(){
               $id_status = $(this).children("option:selected").val();
               if($id_status == 1){
                   $('input#sale').attr('class','');
               }
            });
         });
    </script>
@endsection
	

