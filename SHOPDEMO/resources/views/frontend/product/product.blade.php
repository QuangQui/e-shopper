
<style>
	.container {
	    height: auto;
	    margin-bottom: 20px;
	}
	th {
		text-align: left;
		padding-right: 37px;
		font-size: 15px;
		background: #FE980F;
		color: #fff;
		line-height: 2.5;
		padding-left: 15px;
		}
	td {
	    line-height: 2.5;
	    padding-left: 15px;
	}

	img.icon{
		width: 16px;
	}

</style>

@extends('frontend.layout.index-cart')
@section('title','Product/My-Account')
@section('content')
	<section>
		<div class="container" style="margin-top: 20px;">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>My Product</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="account.php">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<a href="account">Account</a>
										</a>
									</h4>
								</div>								
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<a href="product">My Product</a>
										</a>
									</h4>
								</div>
							</div>
						</div><!--/category-products-->
					
						<!--/brands_products-->
						
						
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
                    <div class="row" style="margin: 20px 0 85px 0px;">
						<div class="col-sm-2"></div>
						<div class="col-sm-10">
							<div class="signup-form" style="width: 618px;"><!--sign up form-->
								@if(session('success'))
									<div class="alert alert-danger alert-dismissible">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
										<h4><i class="icon fa fa-check"></i>Thong bao!</h4>
										{{session('success')}}
									</div>
								@endif
								@if($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach($errors->all() as $error)
												<li>{{$error}}</li>
											@endforeach
										</ul>
									</div>
								@endif
								<div class="features_items"><!--features_items-->
									<table id="datatable" style="border: 1px solid #FE980F">
										<thead>
											<tr role="row">
												<th>ID</th>
												<th>Name</th>
												<th>Price</th>
												<th>Image</th>	 
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											{{-- {{ dd($getArrImage) }} --}}
											@foreach($listProduct as $key => $value)
												<tr role="row">
													<td>{{$key+1}}</td>
													<td>{{$value->name}}</td>
													<td>${{$value->price}}</td>
													<td><img style="width: 50px;height: 50px;padding: 5px;" src="../../upload/product/{{ Auth::user()->id}}/{{ current(json_decode($value['images'])) }} " alt="" /></td>
													<td><a href="{{ route('frontend.editProduct',['id' => $value->id])}}"><img class="icon" src="../../frontend/images/product/edit.png"></a></td>
													<td><a href="{{ route('frontend.deleteProduct',['id' => $value->id ])}}"><img class="icon" src="../../frontend/images/product/delete.png"
														onclick="return confirm('Bạn có muốn xóa không?')"></a></td>			               
												</tr>
											@endforeach
										</tbody>
									</table>
									<div style="height: 30px;margin-top: 10px; float: right;">
										{{ $listProduct->links() }}
									</div>
									<div>
										</br></br></br><a href="{{ route('frontend.addProduct') }}" ><button style="background: #FE980F;border: none;
											color: #fff; padding: 5px; float: right" id="button">Add Product</button></a>
									</div>
								</div><!--features_items-->
							</div><!--/sign up form-->
						</div>
                    </div>
				</div>
			</div>
		</div>
	</section>
@endsection
	

