@extends('frontend.layout.index-cart')
@section('title','Cart')
@section('content')
<div>
    <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			{{-- {{dd($carts)}} --}}
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description">Description</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@php 
								$total = 0;
						@endphp
						@if(isset($carts))
							@foreach($carts as $key => $value)
								@php 
									$sum = $value['price'] * $value['qty'];
									$total += $sum;
								@endphp
								<tr id="{{$key}}">
									<td class="cart_product">
										@if(!auth()->check())
											<a href=""><img src="../../upload/product/16/{{ $value['img'] }}" alt=""></a>
										@else
											<a href=""><img src="../../upload/product/{{ Auth::user()->id}}/{{ $value['img'] }}" alt=""></a>
										@endif
									</td>
									<td class="cart_description">
										<h4><a href="">{{ $value['name'] }}</a></h4>
										<p>Web ID: 1089772</p>
									</td>
									<td class="cart_price">
										<p>${{ $value['price'] }}</p>
									</td>
									<td class="cart_quantity">
										<div class="cart_quantity_button">
											<a class="cart_quantity_up" href=""> + </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="{{ $value['qty'] }}" autocomplete="off" size="2">
											<a class="cart_quantity_down" href=""> - </a>
										</div>
									</td>
									<td class="cart_total">
										<p class="cart_total_price">
										<?php 
											echo "$".$sum;
										?></p>
									</td>
									<td class="cart_delete">
										<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>
							@endforeach
						@else
							<p style="color:red;">Khong co san pham nao!</p>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span class="total"><?php echo "$".$total; ?></span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span class="total"><?php echo "$".$total;?></span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="{{ route('frontend.checkout') }}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('a.cart_quantity_up').click(function(){
				var valID = $(this).closest('tr').attr('id');
				var valPrice = $(this).closest("tr").find("td.cart_price").text();
				valPrice = valPrice.replace("$","");
				var valQty = $(this).closest('div.cart_quantity_button').find('input').attr('value');
				valQty = parseInt(valQty) + 1;
				$(this).closest('div.cart_quantity_button').find('input').attr('value',valQty);
				var sumPrice = valPrice * valQty;
				$(this).closest("tr").find("p.cart_total_price").text("$" +sumPrice);
				$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
				$.ajax({
					type: "POST",
					url: "{{ route('frontend.cartQtyUp')}}",
					data:{ valID:valID },
					success:function(data){
						$('span.total').text("$" +data.total);
					
					}
				});
				return false;
			})

			$('a.cart_quantity_down').click(function(){
				var valID = $(this).closest('tr').attr('id');
				var valPrice = $(this).closest("tr").find("td.cart_price").text();
				valPrice = valPrice.replace("$","");
				var valQty = $(this).closest('div.cart_quantity_button').find('input').attr('value');
				valQty = parseInt(valQty) - 1;
				$(this).closest('div.cart_quantity_button').find('input').attr('value',valQty);
				var sumPrice = valPrice * valQty;
				$(this).closest("tr").find("p.cart_total_price").text("$" +sumPrice);
				var total = 0;
				total -= sumPrice;
				// alert(total);
				total -= total;
				$('span.total').text("$" +total);
				if(valQty < 1){
					$(this).closest('tr').remove();
				}
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					type:"POST",
					url: "{{ route('frontend.cartQtyDown') }}",
					data: {valID:valID},
					success:function(data){
						$('span.total').text("$" +data.total);
					}
				});
				return false;
			})

			$('.cart_quantity_delete').click(function(){
				$(this).closest('tr').remove();
				valID = $(this).closest('tr').attr('id');
				// alert(valID);
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					type:"GET",
					url: "{{ route('frontend.destroyCart') }}",
					data: {valID:valID},
					success:function(data){
						console.log(data);
					}
				});
				return false;
			})
		})
	</script> --}}
</div>
@endsection
