@extends('frontend.layout.index-cart')
@section('title','Checkout')
@section('content')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Check out</li>
            </ol>
        </div><!--/breadcrums-->
        @if(!auth()->check())
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <div class="signup-form"><!--sign up form-->
                        <h2>Please Register!</h2>
                        @if(session('success'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thong bao!</h4>
                                {{session('success')}}
                            </div>
                        @endif
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('frontend.saveHistory') }}" method ="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="name" placeholder="Name"/>
                            <input type="email" name="email" placeholder="Email Address"/>
                            <input type="number" name="phone" placeholder="Phone"/>
                            <input type="password" name="password" placeholder="Password"/>
                            <input type="password" name="password_confirmation" placeholder="Confirm Password"/>
                            <input style="padding-top: 10px;" type="file" name="avatar" id="avatar">
                            <button type="submit" class="btn btn-default">Signup</button>
                        </form>
                    </div><!--/sign up form-->
                </div>
                <div class="col-sm-4"></div>
            </div>
        @endif
        <div class="review-payment">
            <h2>Review & Payment</h2>
        </div>

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $total = 0;
                    @endphp
                    @if(session()->has('cart'))
                        <?php 
                            $carts = session()->get('cart');
                        ?>
                        @foreach($carts as $key => $value)
                            @php 
                                $sum = $value['price'] * $value['qty'];
                                $total += $sum;
                            @endphp
                            <tr id="{{$key}}">
                                <td class="cart_product">
                                    @if(!auth()->check())
                                        <a href=""><img src="../../upload/product/16/{{ $value['img'] }}" alt=""></a>
                                    @else
                                        <a href=""><img src="../../upload/product/{{ Auth::user()->id}}/{{ $value['img'] }}" alt=""></a>
                                    @endif
                                </td>
                                <td class="cart_description">
                                    <h4><a href="">{{ $value['name'] }}</a></h4>
                                    <p>Web ID: 1089772</p>
                                </td>
                                <td class="cart_price">
                                    <p>${{ $value['price'] }}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <a class="cart_quantity_up" href=""> + </a>
                                        <input class="cart_quantity_input" type="text" name="quantity" value="{{ $value['qty'] }}" autocomplete="off" size="2">
                                        <a class="cart_quantity_down" href=""> - </a>
                                    </div>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">
                                    <?php 
                                        echo "$".$sum;
                                    ?></p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <p style="color:red;">Khong co san pham nao!</p>
                    @endif
                    <tr>
                        <td colspan="4">&nbsp;</td>
                        <td colspan="2">
                            <table class="table table-condensed total-result">
                                <tr>
                                    <td>Cart Sub Total</td>
                                    <td><span class="total"><?php echo "$".$total ?></span></td>
                                </tr>
                                <tr>
                                    <td>Exo Tax</td>
                                    <td>$0</td>
                                </tr>
                                <tr class="shipping-cost">
                                    <td>Shipping Cost</td>
                                    <td>Free</td>										
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><span><span class="total"><?php echo "$".$total ?></span></td>
                                </tr>
                                <tr class="btn-order" >
                                    <td>
                                        <a class="btn btn-primary btn_order" href=" {{ route('frontend.sendMail') }}">Place order now</a>  
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="payment-options">
                <span>
                    <label><input type="checkbox"> Direct Bank Transfer</label>
                </span>
                <span>
                    <label><input type="checkbox"> Check Payment</label>
                </span>
                <span>
                    <label><input type="checkbox"> Paypal</label>
                </span>
            </div>
    </div>
</section> <!--/#cart_items-->
@endsection