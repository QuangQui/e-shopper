<!DOCTYPE html>
<html lang=&quot;en-US&quot;>
<head>
    <meta charset=&quot;utf-8&quot;>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .pTag{
            font-size: 16px;
            margin: 0;
            color: black;
            padding: 0;
        }
        
        .cart_info .table.table-condensed.total-result {
            margin-bottom: 10px;
            margin-top: 35px;
            color: #696763
        }

        .cart_info .table.table-condensed.total-result tr {
            border-bottom: 0
        }

         .cart_info .table.table-condensed.total-result span {
            color: #FE980F;
            font-weight: 700;
            font-size: 16px
        }

         .cart_info .table.table-condensed.total-result .shipping-cost {
            border-bottom: 1px solid #F7F7F0;
        }
         .cart_info .cart_menu {
            background: #FE980F;
            color: #fff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: normal;
        }
        .cart_info table tr td {
            border-top: 0 none;
            vertical-align: inherit;
            margin-right: 5px;
        }
        .cart_info {
            border: 1px solid #E6E4DF;
            margin-bottom: 50px;
        }
        .btn.btn-primary {
            background: #FE980F;
            border: 0 none;
            border-radius: 0;
            margin-top: 16px;
        }
        .table {
            width: 100%;
            margin-bottom: 20px;
        }

    </style>
</head>
<body>
    {!! $emailBody !!}
    <br>
    <p class="pTag"> Cảm ơn bạn! </p>
    <br>
    <h4 class="pTag">Chi tiết đơn hàng</h4>
    <br>
    <div class="table-responsive cart_info">
        <table class="table table-condensed">
            <thead>
                <tr class="cart_menu">
                    <td class="description">Description</td>
                    <td class="price">Price</td>
                    <td class="quantity">Quantity</td>
                    <td class="total">Total</td>
                </tr>
            </thead>
            <tbody>
                @php 
                    $total = 0;
                @endphp
                @if(session()->has('cart'))
                    <?php 
                        $carts = session()->get('cart');
                    ?>
                    @foreach($carts as $key => $value)
                        @php 
                            $sum = $value['price'] * $value['qty'];
                            $total += $sum;
                        @endphp
                        <tr id="{{$key}}">
                            <td class="cart_description">
                                <h4><a href="">{{ $value['name'] }}</a></h4>
                                <p>Web ID: 1089772</p>
                            </td>
                            <td class="cart_price">
                                <p>${{ $value['price'] }}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{ $value['qty'] }}" autocomplete="off" size="2">
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">
                                <?php 
                                    echo "$".$sum;
                                ?></p>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <p style="color:red;">Khong co san pham nao!</p>
                @endif
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2">
                        <table class="table table-condensed total-result">
                            <tr>
                                <td>Cart Sub Total</td>
                                <td><?php echo "$".$total ?></td>
                            </tr>
                            <tr>
                                <td>Exo Tax</td>
                                <td>$0</td>
                            </tr>
                            <tr class="shipping-cost">
                                <td>Shipping Cost</td>
                                <td>Free</td>										
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td><span><?php echo "$".$total ?></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>