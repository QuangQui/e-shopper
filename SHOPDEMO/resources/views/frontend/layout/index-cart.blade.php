<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | E-Shopper</title>
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    {{-- <link href="{{asset('frontend/css/price-range.css')}}" rel="stylesheet"> --}}
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!--[if lt IE 9]>
    <script src="{{asset('frontend/js/html5shiv.js')}}"></script>
    <script src="{{asset('frontend/js/respond.min.js')}}"></script>
    <![endif]-->       

<!-- Script -->
    
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<!-- header -->
    @include('frontend.layout.header')
	
	<!-- content -->
    @yield('content')
	<!-- footer -->
	@include('frontend.layout.footer')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('frontend/js/jquery.js')}}"></script>
	<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>
	{{-- <script src="{{asset('frontend/js/price-range.js')}}"></script> --}}
    <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('http://maps.google.com/maps/api/js?sensor=true')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/gmaps.js')}}"></script>
	<script src="{{asset('frontend/js/contact.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('a.cart_quantity_up').click(function(){
				var valID = $(this).closest('tr').attr('id');
				var valPrice = $(this).closest("tr").find("td.cart_price").text();
				valPrice = valPrice.replace("$","");
				var valQty = $(this).closest('div.cart_quantity_button').find('input').attr('value');
				valQty = parseInt(valQty) + 1;
				$(this).closest('div.cart_quantity_button').find('input').attr('value',valQty);
				var sumPrice = valPrice * valQty;
				$(this).closest("tr").find("p.cart_total_price").text("$" +sumPrice);
				$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
				$.ajax({
					type: "POST",
					url: "{{ route('frontend.cartQtyUp')}}",
					data:{ valID:valID },
					success:function(data){
						$('span.total').text("$" +data.total);
					
					}
				});
				return false;
			})

			$('a.cart_quantity_down').click(function(){
				var valID = $(this).closest('tr').attr('id');
				var valPrice = $(this).closest("tr").find("td.cart_price").text();
				valPrice = valPrice.replace("$","");
				var valQty = $(this).closest('div.cart_quantity_button').find('input').attr('value');
				valQty = parseInt(valQty) - 1;
				$(this).closest('div.cart_quantity_button').find('input').attr('value',valQty);
				var sumPrice = valPrice * valQty;
				$(this).closest("tr").find("p.cart_total_price").text("$" +sumPrice);
				// var total = 0;
				// total -= sumPrice;
				// // alert(total);
				// total -= total;
				// $('span.total').text("$" +total);
				if(valQty < 1){
					$(this).closest('tr').remove();
				}
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					type:"POST",
					url: "{{ route('frontend.cartQtyDown') }}",
					data: {valID:valID},
					success:function(data){
						$('span.total').text("$" +data.total);
					}
				});
				return false;
			})

			$('.cart_quantity_delete').click(function(){
				$(this).closest('tr').remove();
				valID = $(this).closest('tr').attr('id');
				// alert(valID);
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					type:"GET",
					url: "{{ route('frontend.destroyCart') }}",
					data: {valID:valID},
					success:function(data){
						console.log(data);
					}
				});
				return false;
			})
		})
	</script>
</body>
</html>