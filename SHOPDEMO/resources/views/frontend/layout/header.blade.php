
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +84877.778.819</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> tqqui.it@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="https://www.facebook.com/profile.php?id=100005276117966"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->
    
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-md-4 clearfix">
                    <div class="logo pull-left">
                        <a href="index.html"><img src="images/home/logo.png" alt="" /></a>
                    </div>
                    <div class="btn-group pull-right clearfix">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                USA
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="">Canada</a></li>
                                <li><a href="">UK</a></li>
                            </ul>
                        </div>
                        
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                DOLLAR
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="">Canadian Dollar</a></li>
                                <li><a href="">Pound</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 clearfix">
                    <div class="shop-menu clearfix pull-right">
                        <ul class="nav navbar-nav">
                            <li>
                                @if(!auth()->check())
                                    <a href="{{route('frontend.login')}}"><i class="fa fa-user"></i> Account</a>
                                @else
                                    <a href="{{route('frontend.showAccount')}}"><i class="fa fa-user"></i>{{Auth::User()->name}}</a>
                                @endif
                            </li>
                            <li><a href=""><i class="fa fa-star"></i> Wishlist</a></li>
                            <li><a href="{{route('frontend.checkout')}}"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                            <li><a href="{{route('frontend.cart')}}"><i class="fa fa-shopping-cart"></i><span class="countCart">
                                <?php
                                if(session()->has('cart')){
                                    $countCart = count(session()->get('cart'));
                                    echo $countCart;
                                } 
                                ?></span>  Cart</a></li>
                            <li>
                                @if(!auth()->check())
                                    <a href="{{route('frontend.login')}}"><i class="fa fa-lock"></i>Login</a>
                                @else
                                    <a href="{{route('frontend.logout')}}"><i class="fa fa-lock"></i>Logout</a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{route('frontend.home')}}" class="active">Home</a></li>
                            <li class="dropdown"><a href="{{route('frontend.showShop')}}">Shop<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="{{route('frontend.home')}}">Products</a></li>
                                    <li><a href="{{route('frontend.home')}}">Product Details</a></li> 
                                    <li><a href="{{route('frontend.checkout')}}">Checkout</a></li> 
                                    <li><a href="{{route('frontend.cart')}}">Cart</a></li> 
                                    <li><a href="{{route('frontend.logout')}}">Logout</a></li> 
                                </ul>
                            </li> 
                            <li><a href="{{route('frontend.blog')}}">Blog</a></li>
                            {{-- <li class="dropdown"><a href="{{route('frontend.blog')}}">Blog<i class="fa fa-angle-down"></i></a>
                                <li><a href="{{route('frontend.blog')}}">Blog List</a></li>
                                {{-- <ul role="menu" class="sub-menu">
                                    {{-- <li><a href="{{route('frontend.blogSingle', )}}">Blog Single</a></li>
                                </ul>
                            </li>  --}}
                            <li><a href="{{route('frontend.error')}}">404</a></li>
                            <li><a href="{{route('frontend.contact')}}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <form action="{{ route('frontend.showShop') }}" method="get">
                            @csrf
                            <input type="text" name="nameProduct" placeholder="Name product"/>
                            {{-- <button style="border: none;background: #fdb45e;height: 33px;color: #ffffff;">Search</button> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->